﻿#include <stdio.h>
#include <stdlib.h>
#include <windows.h>
#include <time.h>
#include <chrono>

void heapify(float arr[], int n, int i) {
    int largest = i;
    int left = 2 * i + 1;
    int right = 2 * i + 2;
    if (left < n && arr[left] > arr[largest])
        largest = left;
    if (right < n && arr[right] > arr[largest])
        largest = right;
    if (largest != i) {
        float temp = arr[i];
        arr[i] = arr[largest];
        arr[largest] = temp;
        heapify(arr, n, largest);
    }
}

void HeapSort(float arr[], int n) {
    for (int i = n / 2 - 1; i >= 0; i--)
        heapify(arr, n, i);
    for (int i = n - 1; i >= 0; i--) {
        float temp = arr[0];
        arr[0] = arr[i];
        arr[i] = temp;
        heapify(arr, i, 0);
    }
}

int sengwik(int n) {
    int sendgewik[] = { 929, 505, 209, 109, 41, 19, 5, 1 };
    int i;
    for (i = 0; i < sizeof(sendgewik) / sizeof(sendgewik[0]); i++) {
        if (sendgewik[i] < n)
            return sendgewik[i];
    }
    return 1;
}

void ShellSort(int arr[], int n) {
    int gap, i, j, temp;

    for (int k = 0; k < n; k++) {
        gap = sengwik(n - k);
        for (i = gap; i < n; i++) {
            temp = arr[i];
            for (j = i; j >= gap && arr[j - gap] > temp; j -= gap)
                arr[j] = arr[j - gap];
            arr[j] = temp;
        }
    }
}

void CountingSort(short arr[], int n) {
    const int range = 31 + 20 + 1;
    int* count = (int*)calloc(range, sizeof(int));
    short* output = (short*)malloc(n * sizeof(short));
    if (count == NULL || output == NULL) {
        printf("Memory allocation error\n");
        free(count);
        free(output);
        return;
    }
    for (int i = 0; i < n; i++)
        count[arr[i] + 20]++;
    for (int i = 1; i < range; i++)
        count[i] += count[i - 1];
    for (int i = n - 1; i >= 0; i--) {
        output[count[arr[i] + 20] - 1] = arr[i];
        count[arr[i] + 20]--;
    }
    for (int i = 0; i < n; i++)
        arr[i] = output[i];
    free(count);
    free(output);
}

int main() {
    srand(time(NULL));
    int sizes[] = { 10, 100, 500, 1000, 2000, 5000, 10000 };
    int num_sizes = sizeof(sizes) / sizeof(sizes[0]);

    for (int i = 0; i < num_sizes; i++) {
        int size = sizes[i];
        float* arr_heap = (float*)malloc(size * sizeof(float));
        int* arr_shell = (int*)malloc(size * sizeof(int));
        short* arr_counting = (short*)malloc(size * sizeof(short));
        for (int j = 0; j < size; j++) {
            arr_heap[j] = -200 + ((float)rand() / RAND_MAX) * (50 - (-200));
           /* printf("%f ", arr_heap[j]);*/
            arr_shell[j] = ((int)rand() / (RAND_MAX / 100 + 1));
            arr_counting[j] = -20 + rand() % 31;
        }

        //// Heap Sort
        auto begin = std::chrono::steady_clock::now();
        HeapSort(arr_heap, size);
      for (int j = 0;j < size;j++) {
            printf("%f", arr_heap[j]);
        }
        auto end = std::chrono::steady_clock::now();
        auto elapsed_ns = std::chrono::duration_cast<std::chrono::nanoseconds>(end - begin);
        printf("Size: %d\n", size);
        printf("Heap Sort (%d elements): %lld nanoseconds\n", size, (elapsed_ns.count()));

        // Shell Sort
        auto begin1 = std::chrono::steady_clock::now();
        ShellSort(arr_shell, size);
       /* for (int j = 0;j < size;j++) {
            printf("%d", arr_shell[j]);
        }*/
        auto end1 = std::chrono::steady_clock::now();
        auto elapsed_ns1 = std::chrono::duration_cast<std::chrono::nanoseconds>(end1 - begin1);
        printf("Shell Sort (%d elements): %lld nanoseconds\n", size, (elapsed_ns1.count()));

        //// Counting Sort
        auto begin2 = std::chrono::steady_clock::now();
        CountingSort(arr_counting, size);
      /*  for (int j = 0;j < size;j++) {
            printf("%d", arr_counting[j]);
        }*/
        auto end2 = std::chrono::steady_clock::now();
        auto elapsed_ns2 = std::chrono::duration_cast<std::chrono::nanoseconds>(end2 - begin2);
        printf("Counting Sort (%d elements): %lld nanoseconds\n", size, (elapsed_ns2.count()));

        free(arr_heap);
        free(arr_shell);
        free(arr_counting);
        printf("\n");
    }
    return 0;
}
